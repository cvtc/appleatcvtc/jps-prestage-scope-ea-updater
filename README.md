JPS PreStage Scope EA Updater
=======================================

Updates the "PreStage Scope" EA in JPS which identifies the PreStage it will use on next enroll. Will not update an EA if it has not changed since the last run. 

## Reasoning for script
There isn't a way to see or use this device criteria in JPS currently other than digging into the scoping in the individual PreStages. It's always been info I wanted and now we had a use case for it with our student offboarding.

## Requirements
This script relies on a mobile device extension attribute being created with the following settings:

Display Name:
```
PreStage Scope
```
Data Type:
```
String
```
Input Type:
```
Text Field
```

## Usage

### Local
Export the variables listed below into your environment:
* All variable values are described below.
* The first command will make your bash history ignore commands with whitespace infront of them. This will make it so your JPS_PASSWORD isn't in 
your terminal history.
```console
setopt HIST_IGNORE_SPACE
export JPS_URL=https://example.jamf.com
export JPS_USERNAME=exampleUsername
 export JPS_PASSWORD=examplePassword
export ADVANCED_SEARCH_NAME="Name of Advanced Search"
export MULTITHREAD_WORKERS=5
```

Run these commands to install all dependencies and run the script:
```console
pip install pipenv
pipenv install --deploy
pipenv run python ./jps_prestage-scope-ea-updater.py
```

### GitLab
Uses base Azure Function setup, consult [general documentation.](https://gitlab.com/cvtc/appleatcvtc/project-templates/-/tree/main/Python%20Azure%20Functions?ref_type=heads)

## Variables

### JPS_URL
* URL of your JPS Server
> https://example.jamfcloud.com

### JPS_USERNAME / JPS_PASSWORD
JPS account credentials with the following permissions set:
* Jamf Pro Server Objects
    * Advanced Mobile Device Searches
        * Read
    * Mobile Device PreStage Enrollments
        * Read
    * Mobile Devices
        * Read
        * Update

### ADVANCED_SEARCH_NAME
Name of a mobile device advanced search with the following set:
* Criteria:
  * Display Name is not ""
* Display:
  * Device:
    * Serial Number
  * Extension Attributes:
    * PreStage Scope

### MULTITHREAD_WORKERS
This is a global variable and does not need to be set in this project.

Number of multithread workers to run Jamf API requests, a higher number will complete the script faster. I would recommend setting it to no higher than 5 because otherwise the JPS will start returning 502 errors on some requests.

## Authors
Bryan Weber (bweber26@cvtc.edu)

## Copyright
JPS PreStage Scope EA Updater is Copyright 2022 Chippewa Valley Technical College. It is provided under the MIT License. For more information, please see the LICENSE file in this repository.