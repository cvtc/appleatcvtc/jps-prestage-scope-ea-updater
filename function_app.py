import azure.functions as func

import jps_prestage_scope_ea_updater

app = func.FunctionApp()


@app.schedule(
    schedule="0 */15 * * * *",
    arg_name="myTimer",
    run_on_startup=False,
    use_monitor=False,
)
def TimerTrigger(myTimer: func.TimerRequest) -> None:
    jps_prestage_scope_ea_updater.main()
