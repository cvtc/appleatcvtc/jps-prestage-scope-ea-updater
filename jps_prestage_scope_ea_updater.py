import logging
from concurrent.futures import ThreadPoolExecutor
from os import environ

from jps_api_wrapper.classic import Classic
from jps_api_wrapper.pro import Pro

logging.basicConfig(
    level=logging.INFO, format="%(asctime)s - %(levelname)s - %(message)s"
)


def main():
    JPS_URL = environ["JPS_URL"]
    JPS_USERNAME = environ["JPS_USERNAME"]
    JPS_PASSWORD = environ["JPS_PASSWORD"]
    ADVANCED_SEARCH_NAME = environ["ADVANCED_SEARCH_NAME"]

    MULTITHREAD_WORKERS = int(environ["MULTITHREAD_WORKERS"])

    with Classic(JPS_URL, JPS_USERNAME, JPS_PASSWORD) as classic, Pro(
        JPS_URL, JPS_USERNAME, JPS_PASSWORD
    ) as pro:
        executor = ThreadPoolExecutor(max_workers=MULTITHREAD_WORKERS)

        devices = classic.get_advanced_mobile_device_search(name=ADVANCED_SEARCH_NAME)[
            "advanced_mobile_device_search"
        ]["mobile_devices"]

        prestage_scopes = pro.get_mobile_device_prestages_scopes()[
            "serialsByPrestageId"
        ]
        prestages = pro.get_mobile_device_prestages()["results"]
        unique_prestages = {
            prestage["id"]: prestage["displayName"] for prestage in prestages
        }
        unique_prestages[""] = "Not scoped to instance"

        """Add prestage info to device dictionaries"""
        for device in devices:
            try:
                device["prestage_id"] = prestage_scopes[device["Serial_Number"]]
            except KeyError:
                device["prestage_id"] = ""

        # Get the ids and eas of devices that need to be updated
        device_ids = []
        device_eas = []
        for device in devices:
            if device["PreStage_Scope"] != unique_prestages[device["prestage_id"]]:
                device_ids.append(device["id"])
                device_eas.append(
                    {"PreStage Scope": unique_prestages[device["prestage_id"]]}
                )

        if device_ids == []:
            logging.info("No devices need to be updated.")
            return

        logging.info("Updating the following devices...")
        logging.info(device_ids)

        results = [
            result
            for result in executor.map(
                pro.update_mobile_device_extension_attributes,
                device_eas,
                device_ids,
            )
        ]

        logging.info(f"Updated {len(results)} devices.")


if __name__ == "__main__":
    main()
